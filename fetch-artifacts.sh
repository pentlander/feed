#!/usr/bin/env sh

set -o errexit
set -o nounset
set -o pipefail

fetch_artifact() {
    file_name="$1"
    job_name="$2"
    token="$3"
    token_header="Private-Token: $token"
    jobs_url='https://gitlab.com/api/v4/projects/19345359/jobs'

    archives="$(wget -q -O - \
      --header "$token_header" \
      "$jobs_url" \
      | jq '.[] | select(.name | startswith("build")) | { name, id, artifacts: .artifacts | .[] } | select(.artifacts.file_type == "archive")')"

    if [ ! -f "$file_name" ]; then
        job_id="$(echo "$archives" | jq --slurp '[ .[] | select(.name == "'"$job_name"'") ] | first | .id')"
        wget --header "$token_header" "${jobs_url}/${job_id}/artifacts/${file_name}"
    else
        echo "File already exists: ${file_name}"
    fi
}

main() {
    token="$1"
    fetch_artifact rss-static build:rust "$token"
    chmod +x rss-static

    fetch_artifact index.css build:css "$token"
}

main "$@"

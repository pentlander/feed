use std::error::Error;
use std::env;
use std::fs;

use feed_rs::parser::parse;
use chrono::DateTime;
use serde::{Serialize, Deserialize};
use handlebars::Handlebars;
use ureq::get;
use url::Url;
use feed_rs::model::Entry;
use std::path::PathBuf;

const CONNECTION_TIMEOUT_MILLIS: u64 = 10_000;
const MAX_ITEMS_PER_FEED: usize = 10;

#[derive(Serialize, Deserialize)]
struct FeedItem {
    title: String,
    link: String,
    image_url: Option<String>,
    host: String,
    publish_time: i64,
}

impl FeedItem {
    fn from(entry: Entry, image_url: Option<String>) -> FeedItem {
        let link = &entry.links[0];
        FeedItem {
            title: entry.title.unwrap().content.to_string(),
            link: link.href.to_string(),
            image_url,
            host: Url::parse(&link.href).ok().unwrap().host_str().unwrap().to_string(),
            publish_time: entry.published.or(entry.updated).unwrap().timestamp(),
        }
    }
}

#[derive(Serialize, Deserialize)]
struct Page {
    items: Vec<FeedItem>,
}

trait LogError<T> {
    fn ok_or_log(self) -> Option<T>;
}

impl <T, E : Error> LogError<T> for Result<T, E> {
    fn ok_or_log(self) -> Option<T> {
        match self {
            Ok(t) => Some(t),
            Err(e) => {
                eprintln!("{}", e);
                None
            }
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().skip(1).collect();
    let urls_path: PathBuf = args.get(0).expect("Must supply path to file with urls.").into();
    let template_path: PathBuf = args.get(1).expect("Must supply path to template.").into();
    let url_file_contents = fs::read_to_string(urls_path)?;
    let urls: Vec<&str> = url_file_contents.lines().collect();
    let template = fs::read_to_string(template_path)?;

    let mut items = urls.iter()
        .filter_map(|&url| get(url).timeout_connect(CONNECTION_TIMEOUT_MILLIS).call().into_string().ok_or_log())
        .filter_map(|rss_body| parse(rss_body.as_bytes()).ok_or_log())
        .flat_map(|feed| {

            let image_url = feed.icon.map(|image| image.uri);
            let mut items = feed.entries;
            items.sort_by(|a, b| {
                b.published.or(b.updated).unwrap().cmp(&a.published.or(a.updated).unwrap())
            });
            items.truncate(MAX_ITEMS_PER_FEED);
            items.into_iter().map(move |item| FeedItem::from(item, image_url.as_ref().map(|url| url.to_owned())))
        })
        .collect::<Vec<FeedItem>>();
    items.sort_by(|a, b| b.publish_time.cmp(&a.publish_time));

    println!("{}", Handlebars::new().render_template(&template, &Page { items })?);
    Ok(())
}
